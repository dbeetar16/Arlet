"""django_project URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/4.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path, include
from apprentice.views import ApprenticeListado, ApprenticeDetalle, ApprenticeCrear, ApprenticeActualizar, ApprenticeEliminar

from django.conf import settings
from django.conf.urls.static import static


urlpatterns = [
    path('admin/', admin.site.urls),
    path("latearrival/" , include("latearrival.urls" )),
    path("course/" , include("course.urls" )),
    path('apprentice/', ApprenticeListado.as_view(template_name = "apprentice/index.html"), name='leer'),
    path('apprentice/detalle/<int:pk>', ApprenticeDetalle.as_view(template_name = "apprentice/detalles.html"), name='detalles'),
    path('apprentice/crear', ApprenticeCrear.as_view(template_name = "apprentice/crear.html"), name='crear'),
    path('apprentice/editar/<int:pk>', ApprenticeActualizar.as_view(template_name = "apprentice/actualizar.html"), name='actualizar'), 
    path('apprentice/eliminar/<int:pk>', ApprenticeEliminar.as_view(), name='eliminar'), 
]
