from django.shortcuts import render

# Create your views here.
# Instanciamos las vistas genéricas de Django 
from django.views.generic import ListView, DetailView 
from django.views.generic.edit import CreateView, UpdateView, DeleteView
 
# Instanciamos el modelo 'Apprentice' para poder usarlo en nuestras Vistas CRUD
from .models import Apprentice

# Nos sirve para redireccionar despues de una acción revertiendo patrones de expresiones regulares 
from django.urls import reverse
 
# Habilitamos el uso de mensajes en Django
from django.contrib import messages 
 
# Habilitamos los mensajes para class-based views 
from django.contrib.messages.views import SuccessMessageMixin 
 
# Habilitamos los formularios en Django
from django import forms

class ApprenticeListado(ListView): 
    model = Apprentice # Llamamos a la clase 'Apprentice' que se encuentra en nuestro archivo 'models.py'

class ApprenticeCrear(SuccessMessageMixin, CreateView): 
    model = Apprentice # Llamamos a la clase 'Apprentice' que se encuentra en nuestro archivo 'models.py'
    form = Apprentice # Definimos nuestro formulario con el nombre de la clase o modelo 'Apprentice'
    fields = "__all__" # Le decimos a Django que muestre todos los campos de la tabla 'Apprentice' de nuestra Base de Datos 
    success_message = 'Apprentice Creado Correctamente !' # Mostramos este Mensaje luego de Crear un Apprentice
 
    # Redireccionamos a la página principal luego de crear un registro o Apprentice
    def get_success_url(self):        
        return reverse('leer') # Redireccionamos a la vista principal 'leer'

class ApprenticeDetalle(DetailView): 
    model = Apprentice # Llamamos a la clase 'Apprentice' que se encuentra en nuestro archivo 'models.py'
    #template_name = "detalles.html"

class ApprenticeActualizar(SuccessMessageMixin, UpdateView): 
    model = Apprentice # Llamamos a la clase 'Apprentice' que se encuentra en nuestro archivo 'models.py' 
    form = Apprentice # Definimos nuestro formulario con el nombre de la clase o modelo 'Apprentice' 
    fields = "__all__" # Le decimos a Django que muestre todos los campos de la tabla 'Apprentice' de nuestra Base de Datos 
    success_message = 'Apprentice Actualizado Correctamente !' # Mostramos este Mensaje luego de Editar un Apprentice 
 
    # Redireccionamos a la página principal luego de actualizar un registro o Apprentice
    def get_success_url(self):               
        return reverse('leer') # Redireccionamos a la vista principal 'leer'

class ApprenticeEliminar(SuccessMessageMixin, DeleteView): 
    model = Apprentice 
    form = Apprentice
    fields = "__all__"     
 
    # Redireccionamos a la página principal luego de eliminar un registro o Apprentice
    def get_success_url(self): 
        success_message = 'Apprentice Eliminado Correctamente !' # Mostramos este Mensaje luego de Editar un Apprentice 
        messages.success (self.request, (success_message))       
        return reverse('leer') # Redireccionamos a la vista principal 'leer'
        
'''
from django.shortcuts import render
from django.views import View
from .models import Apprentice
from django.views.generic import ListView

# Create your views here.
class ApprenticeListView(ListView):
    model = Apprentice
    template_name = "apprentice_list.html"
'''